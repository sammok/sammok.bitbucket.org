/**
 * @api {delete} /blacklist Delete BlackList Members
 * @apiName deleteMembers
 * @apiGroup BlackList
 *
 * @apiParam {String} user User mail address.
 * @apiParam {String[]} blacklist_id_list A list of WhiteList unique ID (Mongodb document uid).
 * @apiParam {String} blacklist_id WhiteList unique ID (Mongodb document uid) in `blacklist_id_list` param.
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X DELETE -H "Content-Type: application/json" -d '{"blacklist_id_list": [{"blacklist_id": "575ac7fc705358c30fb88679"}, {"blacklist_id": "575ac7fd705358c30fb8867a"}]}' "localhost:4000/blacklist?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} result.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result" : "BlackList Members removed"
 *      }
 */