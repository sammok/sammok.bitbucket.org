/**
 * @api {post} /blacklist/ Create New Members
 * @apiName createMembers
 * @apiGroup BlackList
 *
 * @apiParam {String} user User mail address.
 * @apiParam {String[]} memberList A list of Member of BlackList.
 * @apiParam {String} address Member nickname(option) in `memberList` param.
 * @apiParam {String} nickname Member nickname(option) in `memberList` param.
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X POST -H "Content-Type: application/json" -d '{"memberList": [{"address": "auto@juflower.com", "nickname": "automen"}]}' "localhost:4000/blacklist?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} result
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "result" : "BlackList Member created",
 *         "duplicated": [ "auto@juflower.com"]
 *      }
 */