/**
 * @api {get} /blacklist Request BlackList information
 * @apiName getMembers
 * @apiGroup BlackList
 *
 * @apiParam {String} user User mail address.
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X GET "localhost:4000/blacklist?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} _id Id of mail.
 * @apiSuccess {String} address  Member Address.
 * @apiSuccess {String} nickname  Member nickname.
 * @apiSuccess {String} owner Member owner, Mongodb document uid.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     { "result": [
 *         {
 *             "_id" : "573c7e65fee37bc26f79e7e7",
 *             "address" : "sammok2003@juflower.com",
 *             "nickname" : "sam mok",
 *             "owner" : "573c7e0f005a82cd6fb85798",
 *             "__v" : 0
 *          },
 *         {
 *             "_id" : "573c7e65fee37bc26f79e7e8",
 *             "address" : "aotu@juflower.com",
 *             "nickname" : "automen",
 *             "owner" : "573c7e0f005a82cd6fb85798",
 *             "__v" : 0
 *          }
 *      ]}
 */