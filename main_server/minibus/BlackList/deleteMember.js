/**
 * @api {delete} /blacklist/:blacklist_id Delete BlackList Member
 * @apiName deleteMember
 * @apiGroup BlackList
 *
 * @apiParam {String} user User mail address.
 * @apiParam {String} blacklist_id Member unique ID (Mongodb document uid).
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X DELETE "localhost:4000/blacklist/573d67da7f8c863a79292a4b?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} result.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result" : "BlackList Member removed"
 *      }
 */