/**
 * @api {delete} /mailbox/:mail_id Delete Mail
 * @apiName deleteMail
 * @apiGroup Mailbox
 *
 * @apiParam {String} mail_id  Mail unique ID (Mongodb document uid).
 * @apiParam {String} user User mail address.
 *
 * @apiExample {curl} Example usage (Label selected Mails):
 *     curl -i -X DELETE "localhost:4000/mailbox/573d67da7f8c863a71242b2d?user=sammok2003@juflower.com"
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result:" "Mail Deleted"
 *     }
 */