/**
 * @api {put} /mailbox Update Mails
 * @apiName updateMails
 * @apiGroup Mailbox
 *
 * @apiParam {String} user User mail address.
 * @apiParam {String[]} mail_id_list  Mail unique ID (Mongodb document uid) List.
 * @apiParam {String} status value can choose one in list ["move"].
 * @apiParam {String} status_value value for "status" parameter: new Location name if status is move(Inbox|Drafts|Sent Items|Deleted|Trash).
 *
 * @apiExample {curl} Example usage (label selected Mails):
 *     curl -i -X PUT -H "Content-Type: application/json" -d '{ "mail_id_list": ["573d67da7f8c863a79292a4b", "573d67da7f8c863a71242b2d"], "status": "move", "status_value": "Inbox" }' "localhost:4000/mailbox?user=sammok2003@juflower.com"
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result:" "Mails Updated"
 *     }
 */