/**
 * @api {post} /mailbox/drafts Save Mail to Drafts
 * @apiName sendMailToDrafts
 * @apiGroup Mailbox
 *
 * @apiParam {String} user User mail address.
 * @apiParam {String} to  Mail recipient.
 * @apiParam {String} subject  Mail subject.
 * @apiParam {String} body  Mail content body.
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X POST -H "Content-Type: application/json" -d '{ "to": "292404014@juflower.com", "subject": "from main server", "body": "nihao" }' "localhost:4000/mailbox/drafts?user=sammok2003@juflower.com"
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "result:" "Mail saved to Drafts"
 *     }
 */