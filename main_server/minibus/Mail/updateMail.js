/**
 * @api {put} /mailbox/:mail_id Update Mail
 * @apiName updateMail
 * @apiGroup Mailbox
 *
 * @apiParam {String} mail_id Mail unique ID (Mongodb document uid).
 * @apiParam {String} user User mail address.
 * @apiParam {String} from  Mail sender.
 * @apiParam {String} to  Mail recipient.
 * @apiParam {String} subject  Mail subject.
 * @apiParam {String} body  Mail content body.
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X PUT -H "Content-Type: application/json" -d '{ "to": "juflower@juflower.com", "from": "sammok2003@juflower.com", "subject": "updated this mail", "body": "hello!!!" }' "localhost:4000/mailbox/573d67da7f8c863a79292a4b?user=sammok2003@juflower.com"
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result:" "Mail Updated"
 *     }
 */