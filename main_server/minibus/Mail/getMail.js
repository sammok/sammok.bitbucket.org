/**
 * @api {get} /mailbox/:mail_id Request Mail information
 * @apiName getMail
 * @apiGroup Mailbox
 *
 * @apiParam {String} mail_id Mail unique ID (Mongodb document uid).
 * @apiParam {String} user User mail address.
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X GET "localhost:4000/mailbox/573d67da7f8c863a79292a4b?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} _id Id of mail.
 * @apiSuccess {String} from  Mail sender.
 * @apiSuccess {String} to  Mail recipient.
 * @apiSuccess {Date} receivedTime  Mail receivedTime.
 * @apiSuccess {Date} sentTime  Mail sentTime.
 * @apiSuccess {String} subject  Mail subject.
 * @apiSuccess {String} body  Mail content body.
 * @apiSuccess {String} location One of this list [Inbox,  Junk, Email,  Drafts,  Sent Items,  Deleted Items].
 * @apiSuccess {Boolean} read is user have been read this mail.
 * @apiSuccess {Number} SMTPReplyCode 0 as default value, 200 means mail delivering, 250 means mail received by recipient's server, 550 means mail bounced.
 * @apiSuccess {String} owner Mail owner, Mongodb document uid.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     { "result": {
 *             "_id" : "573c7e65fee37bc26f79e7e7",
 *             "from" : "sammok2003@juflower.com",
 *             "to" : "292404014@qq.com",
 *             "receivedTime" : "Sat May 21 2016 18:41:17 GMT+0800 (CST)",
 *             "sentTime" : null,
 *             "subject" : "from main server",
 *             "body" : "nihao",
 *             "location" : "Drafts",
 *             "read": false,
 *             "SMTPReplyCode" : 250,
 *             "owner" : "573c7e0f005a82cd6fb85798",
 *             "__v" : 0
 *          }
 *      }
 */