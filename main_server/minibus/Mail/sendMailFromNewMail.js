/**
 * @api {post} /mailbox Send Mail from new Mail
 * @apiName sendMailFromNewMail
 * @apiGroup Mailbox
 *
 * @apiParam {String} address User mail address.
 * @apiParam {String} to  Mail recipient.
 * @apiParam {String} subject  Mail subject.
 * @apiParam {String} body  Mail content body.
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X POST -H "Content-Type: application/json" -d '{ "to": "292404014@juflower.com", "from": "sammok2003@juflower.com", "subject": "from main server", "body": "nihao" }' "localhost:4000/mailbox?user=sammok2003@juflower.com"
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "result:" "Mail sent, saved to Sent Mails"
 *     }
 */