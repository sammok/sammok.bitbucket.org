/**
 * @api {put} /mailbox/drafts/:mail_id Send Mail from Drafts
 * @apiName sendMailFromDrafts
 * @apiGroup Mailbox
 *
 * @apiParam {String} user User mail address.
 * @apiParam {String} mail_id Mail unique ID (Mongodb document uid).
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X PUT "localhost:4000/mailbox/drafts/573d67da7f8c863a79292a4b?user=sammok2003@hotmail.com"
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "result:" "Mail Sent"
 *     }
 *
 */