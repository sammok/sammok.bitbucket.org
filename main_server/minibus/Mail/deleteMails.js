/**
 * @api {delete} /mailbox Delete Mails
 * @apiName deleteMails
 * @apiGroup Mailbox
 *
 * @apiParam {String} usewr User mail address.
 * @apiParam {String[]} mail_id  a list of Mail id you want to delete.
 *
 * @apiExample {curl} Example usage (Label selected Mails):
 *     curl -i -X DELETE -H "Content-Type: application/json" -d '{ "mail_id_list": ["573d67da7f8c863a79292a4b", "573d67da7f8c863a71242b2d"] }' "localhost:4000/mailbox?user=sammok2003@juflower.com"
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result:" "Mails Deleted"
 *     }
 */