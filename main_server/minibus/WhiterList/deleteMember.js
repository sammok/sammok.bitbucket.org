/**
 * @api {delete} /whitelist/:whitelist_id Delete WhiteList Member
 * @apiName deletemember
 * @apiGroup WhiteList
 *
 * @apiParam {String} user User mail address.
 * @apiParam {String} whitelist_id Member unique ID (Mongodb document uid).
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X DELETE "localhost:4000/whitelist/573d67da7f8c863a79292a4b?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} result.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result" : "WhiteList Member removed"
 *      }
 */