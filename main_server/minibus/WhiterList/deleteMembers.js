/**
 * @api {delete} /whitelist Delete WhiteList Members
 * @apiName deleteMembers
 * @apiGroup WhiteList
 *
 * @apiParam {String} user User mail address.
 * @apiParam {String[]} whitelist_id_list A list of WhiteList unique ID (Mongodb document uid).
 * @apiParam {String} whitelist_id WhiteList unique ID (Mongodb document uid) in `whitelist_id_list` param.
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X DELETE -H "Content-Type: application/json" -d '{"whitelist_id_list": [{"whitelist_id": "575ac7fc705358c30fb88679"}, {"whitelist_id": "575ac7fd705358c30fb8867a"}]}' "localhost:4000/whitelist?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} result.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result" : "WhiteList Members removed"
 *      }
 */