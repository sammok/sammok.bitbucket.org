/**
 * @api {post} /whitelist/ Create New Members
 * @apiName createMembers
 * @apiGroup WhiteList
 *
 * @apiParam {String} user User mail address(query param).
 * @apiParam {String[]} memberList A list of Member of WhiteList.
 * @apiParam {String} address Contact nickname(option).
 * @apiParam {String} nickname Contact nickname(option).
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X POST -H "Content-Type: application/json" -d '{"memberList": [{"address": "auto@juflower.com", "nickname": "automen"}]}' "localhost:4000/whitelist?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} result
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "result" : "WhiteList Member created"
 *      }
 */