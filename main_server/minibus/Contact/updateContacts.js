/**
 * @api {put} /contacts?address=sammok2003@juflower.com Update Contacts
 * @apiName updateContacts
 * @apiGroup Contact
 *
 * @apiParam {String} addressInQueryParams User mail address(The `Real Field` see example query param).
 * @apiParam {String[]} contact_id_list  A list of Contact unique ID (Mongodb document uid).
 * @apiParam {String} address New Contact mail address.
 * @apiParam {String} nickname New Contact nickname(option).
 * @apiParam {String} phone New Contact phone number(option).
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X PUT -H "Content-Type: application/json" -d '{"contact_list": [{"contact_id": "575ab6ac70e209fb092460ba", "address": "auto@juflower.com", "nickname": "automen", "phone": "18516108888"}, {"contact_id": "575ab4ad6872f0180980b997", "address": "heyunen@juflower.com", "nickname": "heyunen", "phone": "18516106666"}]}' "localhost:4000/contacts?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} result
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result" : "Contacts updated"
 *      }
 */