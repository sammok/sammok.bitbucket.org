/**
 * @api {put} /contacts/:contact_id Update Contact
 * @apiName updateContact
 * @apiGroup Contact
 *
 * @apiParam {String} contact_id Contact unique ID (Mongodb document uid).
 * @apiParam {String} addressInQueryParams User mail address(The `Real Field` see example query param).
 * @apiParam {String} address New Contact mail address(option).
 * @apiParam {String} nickname New Contact nickname(option).
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X PUT -H "Content-Type: application/json" -d '{"contact": {"address": "auto@juflower.com", "nickname": "automen", "phone": "18516108888"}}' "localhost:4000/contacts/575ab2796872f0180980b996?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} result
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result" : "Contact updated"
 *      }
 */