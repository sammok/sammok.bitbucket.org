/**
 * @api {post} /contacts/ Create New Contact
 * @apiName createContact
 * @apiGroup Contact
 *
 * @apiParam {String} user User mail address(query param).
 * @apiParam {String} address Contact mail address.
 * @apiParam {String} nickname Contact nickname.
 * @apiParam {String} phone Contact phone number.
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X POST -H "Content-Type: application/json" -d '{"contact": {"address": "auto@juflower.com", "nickname": "automen", "phone": "18516108500"}}' "localhost:4000/contacts?user=sammok2003@juflower.com"
 *
 * @apiSuccessExample 201 Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "result" : "Contact created",
 *         "_id": "573d67da7f8c863a79292a4b"
 *      }
 *
 * @apiErrorExample {JSON} 409 Contact-Already-Exist:
 *     HTTP/1.1 409 Contact already exist
 *     {
 *       "result": "Contact already exist"
 *     }
 *
 */