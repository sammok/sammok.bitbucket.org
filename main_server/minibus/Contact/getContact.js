/**
 * @api {get} /contacts/:contact_id Request Contact information
 * @apiName getContact
 * @apiGroup Contact
 *
 * @apiParam {String} user User mail address.
 * @apiParam {String} contact_id Contact unique ID (Mongodb document uid).
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X GET "localhost:4000/contacts/573d67da7f8c863a79292a4b?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} _id Id of mail.
 * @apiSuccess {String} address  Mail Address.
 * @apiSuccess {String} nickname  Contact nickname.
 * @apiSuccess {String} owner Contact owner, Mongodb document uid.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     { "result": {
 *             "_id" : "573c7e65fee37bc26f79e7e7",
 *             "address" : "sammok2003@juflower.com",
 *             "nickname" : "sam mok",
 *             "owner" : "573c7e0f005a82cd6fb85798",
 *             "__v" : 0
 *          }
 *     }
 */