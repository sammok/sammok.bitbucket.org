/**
 * @api {delete} /contacts/:contact_id Delete Contact
 * @apiName deleteContact
 * @apiGroup Contact
 *
 * @apiParam {String} user User mail address.
 * @apiParam {String} contact_id Contact unique ID (Mongodb document uid).
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X DELETE "localhost:4000/contacts/573d67da7f8c863a79292a4b?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} result.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result" : "Contact removed"
 *      }
 */