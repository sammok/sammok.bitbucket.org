/**
 * @api {delete} /contacts Delete Contacts
 * @apiName deleteContacts
 * @apiGroup Contact
 *
 * @apiParam {String} user User mail address.
 * @apiParam {String[]} contact_list A list of Contact unique ID (Mongodb document uid).
 * @apiParam {String} contact_id Contact unique ID (Mongodb document uid) in `contact_list` param.
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X DELETE -H "Content-Type: application/json" -d '{"contact_list": [{"contact_id": "575ac7fc705358c30fb88679"}, {"contact_id": "575ac7fd705358c30fb8867a"}]}' "localhost:4000/contacts?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} result.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result" : "Contacts removed"
 *      }
 */