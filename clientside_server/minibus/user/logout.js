/**
 * @api {get} /user/login Logout
 * @apiName logout
 * @apiGroup User
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X GET "localhost:3000/user/logout"
 *
 * @apiSuccess {String} result
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result" : "Logout success"
 *      }
 */
