/**
 * @api {delete} /user/byebye Remove User
 * @apiName removeUser
 * @apiGroup User
 *
 * @apiParam {JSON} user User Mail address.
 * 
 * @apiExample {curl} Example usage:
 *     curl -i -X DELETE "localhost:3000/user/byebye?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} address address in query param.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result" : "User removed."
 *      }
 */