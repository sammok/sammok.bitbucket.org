/**
 * @api {put} /user Update User
 * @apiName updateUser
 * @apiGroup User
 *
 * @apiParam {String} userInParams User mail address in query params.
 * @apiParam {JSON} user User Object.
 * @apiParam {String} address User mail address in field "user".
 * @apiParam {String} password User password in field "user".
 * @apiParam {JSON} security User security in field "user".
 * @apiParam {String} question Security question in field "user".
 * @apiParam {String} answer Security answer in field "user".
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X PUT -H "Content-Type: application/json" -d '{"user": {"password": "a123456_update", "security": {"question": "update name", "answer": "updated answer"}}}' "localhost:3000/user?user=sammok2003@juflower.com"
 *
 * @apiSuccess {String} result
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "result" : "User updated"
 *      }
 */