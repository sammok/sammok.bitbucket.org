/**
 * @api {post} /user/ Create New User
 * @apiName createUser
 * @apiGroup User
 *
 * @apiParam {JSON} user User Object.
 * @apiParam {String} address User mail address in field "user".
 * @apiParam {String} nickname User nickname in field "user".
 * @apiParam {JSON} security User security in field "user".
 * @apiParam {String} password User password in field "user".
 * @apiParam {String} question Security question in field "user".
 * @apiParam {String} answer Security answer in field "user".
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X POST -H "Content-Type: application/json" -d '{"user": {"address": "sammok2003@juflower.com", "nickname": "菊花", "password": "a123456..", "security": {"question": "name", "answer": "sam"}}}' "localhost:3000/user/"
 *
 * @apiSuccess {String} result
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "result" : "User created"
 *      }
 */