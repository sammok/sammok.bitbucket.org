/**
 * @api {get} /user/ Get User
 * @apiName getUser
 * @apiGroup User
 *
 * @apiParam {JSON} user User Mail address.
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X GET "localhost:3000/user?user=sammok2003@juflower.com"
 *
 * @apiSuccess {JSON} result User Object.
 * @apiSuccess {String} address User address in field "result".
 * @apiSuccess {String} nickname User nickname in field "result".
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "result" : {
 *             "address" : "sammok2003@juflower.com",
 *             "nickname" : "sam mok",
 *         }
 *      }
 */