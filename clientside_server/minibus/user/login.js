/**
 * @api {post} /user/login Login
 * @apiName login
 * @apiGroup User
 *
 * @apiParam {JSON} user User Object.
 * @apiParam {String} address User mail address in field "user".
 * @apiParam {String} password User password in field "user".
 *
 * @apiExample {curl} Example usage:
 *     curl -i -X POST -H "Content-Type: application/json" -d '{"user": {"address": "sammok2003@juflower.com", "password": "a123456.."}}' "localhost:3000/user/login"
 *
 * @apiSuccess {String} result
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "result" : "Login success"
 *      }
 */
